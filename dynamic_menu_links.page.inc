<?php

/**
 * @file
 * File for Dynamic Menu Links configuration page.
 */

/**
 * Implements hook_form().
 */
function dynamic_menu_links_form($form, &$form_state) {

  $form = array();
  $form['#attributes']['id'] = 'dynamic_menu_links_form';

  $form['path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add Dynamic Menu Links'),
    '#attributes' => array('class' => array('menu-links-list')),
    '#collapsible' => FALSE,
    '#collapsed' => TRUE,
  );
  $form['path']['menu_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu Path'),
    '#description' => t('Enter the path to use in menus.'),
  );

  $form['path']['target_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Target Path'),
    '#description' => t('Enter the target path for the menu path redirection.'),
  );

  $form['path']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dynamic Menu Links List'),
    '#attributes' => array('class' => array('menu-links-list')),
    '#collapsible' => FALSE,
    '#collapsed' => TRUE,
  );

  // Create list containing all dynamic menu links.
  $query = db_select('node', 'n')->addTag('node_access');
  $query->fields('n', array('title', 'nid'));
  $query->condition('n.type', 'dynamic_menu_link');
  $nodes = $query->execute()->fetchAll();

  if ($nodes != NULL) {
    foreach ($nodes as $node) {
      $nid = $node->nid;
      $form['list']['menu_link_' . $nid] = array(
        '#type' => 'markup',
        '#markup' => '<span class="menu-link-text">' . t('Menu path:') . '</span>' . $node->title,
        '#prefix' => '<div class="dynamic-menu-links-raw">',
      );
      // Submit Button to delete menu link.
      $form['list']['delete_' . $nid] = array(
        '#type' => 'submit',
        '#submit' => array('dynamic_menu_links_delete'),
        '#value' => t('Delete'),
        '#name' => 'submit-' . $nid,
        '#attributes' => array('class' => array('delete-dynamic-menu-link')),
      );
      // Submit Button to edit menu link.
      $form['list']['edit_' . $nid] = array(
        '#type' => 'submit',
        '#submit' => array('dynamic_menu_links_edit'),
        '#value' => t('Edit'),
        '#name' => 'edit-' . $nid,
        '#suffix' => '</div>',
        '#attributes' => array('class' => array('edit-dynamic-menu-link')),
      );
    }
  }
  else {
    $form['list']['empty'] = array(
      '#type' => 'item',
      '#markup' => t('Dynamic Menu Links List is empty.'),
    );
  }

  return $form;
}

/**
 * Dynamic menu links form validation.
 */
function dynamic_menu_links_form_validate($form, &$form_state) {

  // Checking values if save button is pushed.
  if ($form_state['clicked_button']['#value'] == 'Save') {
    $menu_path = $form_state['values']['menu_path'];
    $target_path = $form_state['values']['target_path'];

    // Check if empty values are provided.
    if (!$menu_path) {
      form_set_error('menu_path', t('Menu path is required.'));
    }
    if (!$target_path) {
      form_set_error('target_path', t('Target path is required.'));
    }

    // Check if inputs are different.
    if ($menu_path == $target_path) {
      form_set_error('target_path', t('Target path must be different from menu path.'));
    }

    // Allow only alphanumeric values dashes and underscores.
    $invalid_menu_path = preg_match('/[^\/\-A-Za-z0-9]/', $menu_path, $matches);
    if ($invalid_menu_path != 0) {
      form_set_error('menu_path', t('Invalid menu path.'));
    }

    $invalid_target_path = preg_match('/[^\/\-A-Za-z0-9]/', $target_path, $matches);
    if ($invalid_target_path != 0) {
      form_set_error('target_path', t('Invalid target path.'));
    }

    // Check if target path is a system path or alias.
    $alias_exist = drupal_lookup_path('alias', $target_path);
    $path_exist = drupal_lookup_path('source', $target_path);
    if ($alias_exist == FALSE && $path_exist == FALSE) {
      form_set_error('target_path', t('Invalid target path.'));
    }

    // Check if given menu path already exists.
    $path = db_select('node', 'n')->fields('n', array('title'))
      ->condition('n.title', $menu_path)
      ->condition('n.type', 'dynamic_menu_links');
    $result = $path->execute()->fetchAll();

    if ($result != NULL) {
      form_set_error('menu_path', t('You have already selected this path please choose another one.'));
    }
  }
}

/**
 * Dynamic menu links submit handler.
 */
function dynamic_menu_links_form_submit($form, &$form_state) {
  global $user;

  $menu_path = $form_state['values']['menu_path'];
  $target_path = $form_state['values']['target_path'];

  // Strip all whitespaces.
  $menu_path = preg_replace('/\s+/', '', $menu_path);
  $target_path = preg_replace('/\s+/', '', $target_path);

  // Create content to store values using the entity api.
  $values = array(
    'type' => 'dynamic_menu_link',
    'uid' => $user->uid,
    'comment' => 0,
    'promote' => 0,
  );
  $entity = entity_create('node', $values);
  $wrapper = entity_metadata_wrapper('node', $entity);
  $wrapper->title->set($menu_path);
  $wrapper->field_dynamic_target_path->set($target_path);
  $wrapper->save();

  drupal_set_message(t('You successfully added your dynamic menu link path. Remember to clear your caches before you use it.'));
}

/**
 * Submit handler for delete button.
 */
function dynamic_menu_links_delete($form, &$form_state) {
  // Get nid from triggering element.
  $nid = str_replace('submit-', '', $form_state['triggering_element']['#name']);

  // Delete selected content.
  node_delete($nid);
  drupal_set_message(t('You successfully deleted your custom menu link.'));
}

/**
 * Submit handler for edit button.
 */
function dynamic_menu_links_edit($form, &$form_state) {
  // Get nid from triggering element.
  $nid = str_replace('edit-', '', $form_state['triggering_element']['#name']);

  // Go to node/edit page.
  drupal_goto('node/' . $nid . '/edit');
}
