README.txt
==========

The Problem
==============
When someone tries to save a menu link, this can be an internal Drupal path such
as node/add or an external URL such as http://drupal.org.
If someone try to add a path alias, the following message appears: "Drupal menu
system stores system paths only, but will use the URL alias for display.
path_alias has been stored as node/644.".

Goals
=========
Sometimes is necessary to store url alias as menu links, for example when
sharing your code with a team using features module. This module provides an
alternative solution for this issue.
Dynamic menu links module allows a path of your choice to be saved as menu link
in Drupal menu System instead of path alias.

As an example when you export menu links code with features the following code
appears in menu links export.
'link_path' => 'node/644',
'router_path' => 'node/%',

Where link path is a node id that most of the times is different between
different sites. And when you use features to export your code this is causing
a problem.

But when you use Dynamic Menu Links, your code will be:
'link_path' => 'my_path',
'router_path' => 'my_path',

and the problem is solved.


Documentation
=================
All you have to do is to provide the path of your choice along with a target
path in order to redirect you in the page you want.
There is a configuration page at /admin/structure/dynamic-menu-links where users
can view and delete custom menu links from an available list. They can provide a
menu path along with target path. That menu path can be used in drupal
menu links as path and the module is responsible to redirect the user to the
target path.

The menu path can be valid drupal url path.
The target path must be an existing site path or alias.

This module is compatible with pathauto.

AUTHOR/MAINTAINER
======================
Christina Kaloudi(Christina Kal) (https://drupal.org/user/2793535)
